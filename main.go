package main

import (
	json2 "encoding/json"
	"fmt"
	"github.com/hokaccha/go-prettyjson"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/mem"
	"runtime"
	"strconv"
)

var sysinfo = make(map[string]string)

func handleError(err error) {
	if err != nil {
		fmt.Println(err)
		//os.Exit(-1)
	}
}

func main() {
	sysinfo["program_name"] = "system-info"

	// Detect OS
	sysinfo["os"] = runtime.GOOS
	// memory
	vmStat, err := mem.VirtualMemory()
	handleError(err)

	diskStat, err := disk.Usage("/")
	handleError(err)

	// cpu - get CPU number of cores and speed
	cpuStat, err := cpu.Info()
	handleError(err)

	percentage, err := cpu.Percent(0, true)
	handleError(err)

	//fmt.Println(percentage)

	// host or machine kernel, uptime, platform Info
	hostStat, err := host.Info()
	handleError(err)

	sysinfo["total_memory"] = strconv.FormatUint(vmStat.Total, 10)
	sysinfo["free_memory"] = strconv.FormatUint(vmStat.Free, 10)
	sysinfo["percent_used_memory"] = strconv.FormatFloat(vmStat.UsedPercent, 'f', 2, 64)

	sysinfo["total_disk"] = strconv.FormatUint(diskStat.Total, 10)
	sysinfo["used_disk"] = strconv.FormatUint(diskStat.Used, 10)
	sysinfo["free_disk"] = strconv.FormatUint(diskStat.Free, 10)
	sysinfo["percent_used_disk"] = strconv.FormatFloat(diskStat.UsedPercent, 'f', 2, 64)

	// CPU data
	var cpuJSON, _ = json2.Marshal(cpuStat)
	sysinfo["cpu_data"] = string(cpuJSON)

	// CPU Load
	cpuLoad := make(map[int]string)
	for index, cpupercent := range percentage {
		cpuLoad[index] = strconv.FormatFloat(cpupercent, 'f', 2, 64) + "%"
	}
	var cpuLoadJSON, _ = json2.Marshal(cpuLoad)
	sysinfo["cpu_load"] = string(cpuLoadJSON)

	sysinfo["hostname"] = hostStat.Hostname
	sysinfo["uptime"] = strconv.FormatUint(hostStat.Uptime, 10)
	sysinfo["process_count"] = strconv.FormatUint(hostStat.Procs, 10)

	//fmt.Println(load.Avg())

	//fmt.Println(sysinfo)
	json, _ := prettyjson.Marshal(sysinfo)
	fmt.Println(string(json))
}
